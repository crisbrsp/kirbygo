# kirbyGo

**kirbyGo** is nothing more than a coding exercise: my goal is to use the [Comic Vine API](https://comicvine.gamespot.com/api/) to retrieve information about the amazing [Jack Kirby](https://en.wikipedia.org/wiki/Jack_Kirby) a.k.a. "The King of Comics", namely how many characters he created for Marvel Comics and DC Comics.

I use [Go Programming Language](https://golang.org/) and intend to investigate/play around with the following areas:

- [I/O](https://golang.org/pkg/io/)
- [JSON](https://blog.golang.org/json-and-go)
- [Goroutines](https://tour.golang.org/concurrency/1) and [Channels](https://tour.golang.org/concurrency/2)

## Jack Kirby

In my opinion Jack Kirby was one of the most creative minds of the last century. He created and co-created some of the most popular comic book characters and most important stories for both Marvel and DC Comics, who nowadays gain literally billions of dollars with his work through their movie partners.

I recommend the following links for more information on Jack Kirby:

- [Jack Kirby Biography](https://kirbymuseum.org/biography/)
- [Jack Kirby Interview - The Comics Journal](http://www.tcj.com/jack-kirby-interview/)
- [Kirby: King of Comics](https://www.amazon.com/Kirby-Comics-Anniversary-Mark-Evanier/dp/1419727494)

## Usage of the API

Two resouces of the Comic Vine API are used:

1. [`URL:/person`](https://comicvine.gamespot.com/api/documentation#toc-0-20) to retrieve information about Jack Kirby, including the list of comic book characters created by him
2. [`URL:/character`](https://comicvine.gamespot.com/api/documentation#toc-0-2) to retrieve detailed information about a specific comic book character, including the publishing house that owns it

Considering that the Comic Vine API has a rate limit currently of 200 requests per hour/user, and also considering that Jack Kirby created more than 800 characters in his rich career, the processing aimed in this exercise take some hours to complete.

## Configuration

Your Comic Vine API key must be inserted in file `conf/keyfile`.

## Running

Due to the rate limit of the API usage, the code is prepared by default to send and process the result of [42](https://simple.wikipedia.org/wiki/42_(answer)) requests of `URL:/character`. In order to do that, just type:

    ./kirbyGo

However, it's also possible to process all characters created by Kirby. In order to do that, just type:

    ./kirbyGo -all

## Processing

The counting is done concurrently by three "workers" that receive requests of `URL:/character` to get detailed information about characters and determine the publishing house they belong to. Each worker sleeps for 1 hour after 64 requests, which means that together they send 192 of such requests.

Additionally, considering that Timely Comics, founded in 1939, would evolve by the 1960s to become Marvel Comics, the code counts every character created by Kirby for Timely as belonging to "Marvel".

## Results

According to the Comic Vine API as used in this coding exercise, Jack Kirby created 439 characters for Marvel Comics and 267 characters for DC Comics. 

The overall log output and results are registered in file `results.txt`.

## Copyright and license

MIT licensed.
