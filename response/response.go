package response

// Response represents the response of Comic Vine API REST operation
type Response struct {
	ErrorCode          string `json:"error"`
	Limit              int    `json:"limit"`
	Offset             int    `json:"offset"`
	NumberPageResults  int    `json:"number_of_page_results"`
	NumberTotalResults int    `json:"number_of_total_results"`
	StatusCode         int    `json:"status_code"`
	Version            string `json:"version"`
}

// Person represents the response of Comic Vine API REST GET "person"
type Person struct {
	Response
	Creator Creator `json:"results"`
}

// Creator defines the structure to hold information about comic book creator
type Creator struct {
	CreatedCharacters []CreatedCharacter `json:"created_characters"`
	Name              string             `json:"name"`
}

// CreatedCharacter defines the structure to hold information about created comic book character
type CreatedCharacter struct {
	APIDetailURL  string `json:"api_detail_url"`
	ID            int    `json:"id"`
	Name          string `json:"name"`
	SiteDetailURL string `json:"site_detail_url"`
}

// Character represents the response of Comic Vine API REST GET "character"
type Character struct {
	Response
	CharacterDetail CharacterDetail `json:"results"`
}

// CharacterDetail defines the structure to hold information about comic book character
type CharacterDetail struct {
	Name      string    `json:"name"`
	Publisher Publisher `json:"publisher"`
}

// Publisher defines the structure to hold information about comic book publisher
type Publisher struct {
	APIDetailURL string `json:"api_detail_url"`
	ID           int    `json:"id"`
	Name         string `json:"name"`
}
