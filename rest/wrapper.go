package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	resp "kirbygo/response"
	"log"
	"net/http"
	"time"
)

const (
	baseURL   = "https://comicvine.gamespot.com/api/"
	personURL = baseURL + "person/"
	apiKey    = "?api_key="
	format    = "&format=json"
)

// Wrapper defines structure that wraps execution of REST operations
type Wrapper struct {
	comicVineKey string
}

// GetCreator executes Comic Vine REST GET "person" for the given identification
func (w *Wrapper) GetCreator(ID string) (*resp.Creator, error) {

	// check if Comic Vine API key is already there
	if w.comicVineKey == "" {
		w.loadKey()
	}

	fieldList := "&field_list=name,created_characters"

	// executing REST GET
	restCall := fmt.Sprintf("%s%s/%s%s%s%s", personURL, ID, apiKey, w.comicVineKey, format, fieldList)

	data, err := w.executeGETCall(restCall)

	if err != nil {
		log.Fatalln("Error while retrieving comic book creator:", err)
		return nil, err
	}

	// unmarshalling response
	var person resp.Person

	err = json.Unmarshal(data, &person)

	if err != nil {
		log.Fatalln("Error while unmarshalling comic book creator data:", err)
		return nil, err
	}

	// we are fine!
	return &person.Creator, nil
}

// GetCharacterDetail executes Comic Vine REST GET "character" given as parameter
func (w *Wrapper) GetCharacterDetail(characterURL string) (*resp.CharacterDetail, error) {

	// check if Comic Vine API key is already there
	if w.comicVineKey == "" {
		w.loadKey()
	}

	fieldList := "&field_list=name,publisher"

	// executing REST GET
	restCall := fmt.Sprintf("%s%s%s%s%s", characterURL, apiKey, w.comicVineKey, format, fieldList)

	data, err := w.executeGETCall(restCall)

	if err != nil {
		log.Fatalln("Error while retrieving character:", err)
		return nil, err
	}

	// unmarshalling response
	var character resp.Character

	err = json.Unmarshal(data, &character)

	if err != nil {
		log.Fatalln("Error while unmarshalling comic book creator data:", err)
		return nil, err
	}

	// we are fine!
	return &character.CharacterDetail, nil
}

// executeGETCall encapsulates logic of executing REST GET calls
func (w *Wrapper) executeGETCall(restCall string) ([]byte, error) {

	httpClient := http.Client{Timeout: time.Duration(30 * time.Second)}

	response, err := httpClient.Get(restCall)

	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return nil, err
	}

	return data, nil
}

// loadKey loads Comic Vine Key from file
func (w *Wrapper) loadKey() error {

	data, err := ioutil.ReadFile("conf/keyfile")

	if err != nil {
		log.Fatalln("Error while reading Comic Vine key file:", err)
		return err
	}

	w.comicVineKey = string(data)

	return nil
}
