package main

import (
	"flag"
	"fmt"
	rest "kirbygo/rest"
	utils "kirbygo/utils"
	"log"
)

func main() {

	// parsing input arguments
	allCharacters := flag.Bool("all", false, "all characters")
	flag.Parse()

	wrapper := rest.Wrapper{}

	log.Println("Retrieving information about Jack Kirby")

	creator, err := wrapper.GetCreator("4040-5614")

	if err != nil {
		panic(err)
	}

	log.Println("Number of characters Jack Kirby created:", len(creator.CreatedCharacters))
	log.Println("Computing the number of characters Jack Kirby created for Marvel and DC Comics")

	counter := utils.CharacterCounter{NumberOfWorkers: 3, NumberOfRequests: 42}

	if *allCharacters {
		counter = utils.CharacterCounter{NumberOfWorkers: 3, NumberOfRequests: len(creator.CreatedCharacters)}
	}

	log.Println(fmt.Sprintf("Processing will consider %d created characters", counter.NumberOfRequests))

	fmt.Println()

	result := counter.CountCharactersPerPublisher(creator.CreatedCharacters)

	fmt.Println()

	log.Println("Number of characters Jack Kirby created for Marvel:", result[utils.Marvel])
	log.Println("Number of characters Jack Kirby created for DC Comics:", result[utils.DC])
}
