package counter

import (
	"fmt"
	resp "kirbygo/response"
	"kirbygo/rest"
	"log"
	"sync"
	"time"
)

// Publisher is a type representing a publishing house
type Publisher string

const (
	// Marvel publishing house
	Marvel Publisher = "Marvel"
	// Timely publishing house, who became Marvel in the 1960s
	Timely Publisher = "Timely"
	// DC Comics publishing house
	DC Publisher = "DC Comics"
)

// CharacterCounter defines basic properties used to to count characters
type CharacterCounter struct {
	NumberOfWorkers  int
	NumberOfRequests int
}

// CountCharactersPerPublisher process assynchronously the number of characters per publisher
func (cc *CharacterCounter) CountCharactersPerPublisher(characters []resp.CreatedCharacter) map[Publisher]int {

	// create channel that will register all counting results
	resultChannel := make(chan map[Publisher]int)

	// create channel that will feed the workers
	workersChannel := make(chan string)

	// create workgroup for syncing the workers
	var workgroup sync.WaitGroup

	workgroup.Add(cc.NumberOfWorkers)

	// creating the workers (as goroutines)
	for i := 0; i < cc.NumberOfWorkers; i++ {

		go func(workerID int) {

			defer workgroup.Done()

			err := cc.counterWorker(workerID, workersChannel, resultChannel)

			if err != nil {
				msg := "Worker %d failed."
				log.Printf(msg, workerID)
				log.Fatalln(msg)
			}
		}(i + 1)
	}

	// close the result channel when workers are done
	go func() {
		workgroup.Wait()
		close(resultChannel)
	}()

	// send input to the workers via channel
	for i := 0; i < cc.NumberOfRequests; i++ {
		workersChannel <- characters[i].APIDetailURL
	}

	// computing the overall results
	close(workersChannel)

	// getting the result
	return cc.computeCharactersPerPublisher(resultChannel)
}

// counterWorker encapsulates the logic of a counting worker
func (cc *CharacterCounter) counterWorker(workerID int, workersChannel chan string, resultChannel chan map[Publisher]int) error {

	wrapper := rest.Wrapper{}

	result := make(map[Publisher]int)

	numberOfProcessedCharacters := 0

	for apiURL := range workersChannel {

		log.Println(fmt.Sprintf("Worker %d executing API URL %s", workerID, apiURL))

		characterDetail, err := wrapper.GetCharacterDetail(apiURL)

		if err != nil {
			log.Fatalln("Error while getting details of the character:", err)
			return err
		}

		//log.Println("Character: ", characterDetail)

		if characterDetail.Publisher.Name == string(Marvel) || characterDetail.Publisher.Name == string(Timely) {
			result[Marvel]++
		} else if characterDetail.Publisher.Name == string(DC) {
			result[DC]++
		}

		// throttling down the use of the API due to rate limit
		time.Sleep(1 * time.Second)

		numberOfProcessedCharacters++

		if numberOfProcessedCharacters%64 == 0 {

			log.Println(fmt.Sprintf("Worker %d processed %d characters", workerID, numberOfProcessedCharacters))
			log.Println(fmt.Sprintf("Worker %d will sleep for 1 hour", workerID))

			time.Sleep(1 * time.Hour)
		}
	}

	resultChannel <- result

	return nil
}

// computeCharactersPerPublisher computes the overall results, given the channel
func (cc *CharacterCounter) computeCharactersPerPublisher(channel chan map[Publisher]int) map[Publisher]int {

	result := make(map[Publisher]int)

	// Process results
	for r := range channel {
		result[Marvel] += r[Marvel]
		result[DC] += r[DC]
	}

	return result
}
